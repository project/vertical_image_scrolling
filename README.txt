CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module will create the vertical scroll slideshow on your 
drupal BLOCK LAYOUT. module will create the slideshow like reel. 
The images will scroll one by one from bottom to top. Each slide 
can be optionally hyperlinked. Options available in BLOCK 
configuration to set the images in random order.

Live Demo
http://www.gopiplus.com/
extensions/2017/04/drupal-module-vertical-image-scrolling/

FEATURES:
---------

1. Easy to customize.
2. Support all browser.
3. Hyperlink option to each images.
4. Random order image display option.

REQUIREMENTS
------------

Just vertical image scrolling moudule files


INSTALLATION
------------

1. Install the module as normal, see link for instructions.
Link: https://www.drupal.org/documentation/install/
modules-themes/modules-8

CONFIGURATION
-------------

1. Go to Admin >> Extend page and check the Enabled check box near 
to the module Vertical image scrolling and then click the Install
button at the bottom.

2. Go to Admin >> Structure >> Block layout page, there you can 
see button called Place Block near each available blocks.

3. Go to Admin >> Structure >> Blocks layout page and click Place 
Block button to add Block. If you want to add Block in your Sidebar first,
click Place Block button near your Sidebar first title. It will open 
Place block window. In that window again click Place Block button near 
Vertical image scrolling.

4. Now open your website front end and see Vertical image scrolling 
module in the selected location. go to configuration link of the module 
and update the default message.

MAINTAINERS
-----------

Current maintainers:

 * Gopi RAMASAMY 
https://www.drupal.org/user/1388160
http://www.gopiplus.com/work/about/
http://www.gopiplus.com/extensions/2017/04/
	drupal-module-vertical-image-scrolling/
 
Requires - Drupal 8
License - GPL (see LICENSE)
