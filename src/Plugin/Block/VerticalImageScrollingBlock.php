<?php

namespace Drupal\vertical_image_scrolling\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'VerticalImageScrolling' Block.
 *
 * @Block(
 * id = "vertical_image_scrolling",
 * admin_label = @Translation("Vertical image scrolling"),
 * )
 */
class VerticalImageScrollingBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['vis_display'])) {
      $vis_display = $config['vis_display'];
      if (!is_numeric($vis_display)) {
        $vis_display = 2;
      }
    }
    else {
      $vis_display = 2;
    }

    if (!empty($config['vis_height'])) {
      $vis_height = $config['vis_height'];
      if (!is_numeric($vis_height)) {
        $vis_height = 170;
      }
    }
    else {
      $vis_height = 170;
    }

    if (!empty($config['vis_speed'])) {
      $vis_speed = $config['vis_speed'];
      if (!is_numeric($vis_speed)) {
        $vis_speed = 2;
      }
    }
    else {
      $vis_speed = 2;
    }

    if (!empty($config['vis_waitsec'])) {
      $vis_waitsec = $config['vis_waitsec'];
      if (!is_numeric($vis_waitsec)) {
        $vis_waitsec = 2;
      }
    }
    else {
      $vis_waitsec = 2;
    }

    if (!empty($config['vis_random'])) {
      $vis_random = $config['vis_random'];
      if ($vis_random != "YES" && $vis_random != "NO") {
        $vis_random = "NO";
      }
    }
    else {
      $vis_random = "NO";
    }

    if (!empty($config['vis_images'])) {
      $vis_images = $config['vis_images'];
    }
    else {
      $vis_images = "";
    }

    $output[]['#cache']['max-age'] = 0;
    $values = [
      'vis_display' => $vis_display,
      'vis_height' => $vis_height,
      'vis_speed' => $vis_speed,
      'vis_waitsec' => $vis_waitsec,
      'vis_random' => $vis_random,
      'vis_images' => $vis_images,
    ];

    $markup = $this->visBlock($values);
    $output[] = [
      '#markup' => $markup,
      '#allowed_tags' => ['script', 'div', 'a', 'table', 'ilayer', 'layer'],
    ];
    $output['#attached']['library'][] = 'vertical_image_scrolling/vertical_image_scrolling';
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  private function visBlock(array $values) {
    $vis_display = $values['vis_display'];
    $vis_height = $values['vis_height'];
    $vis_speed = $values['vis_speed'];
    $vis_waitsec = $values['vis_waitsec'];
    $vis_random = $values['vis_random'];
    $vis_images = $values['vis_images'];

    if ($vis_images == "") {
      $his_dtl = "image details are not available.";
      return $his_dtl;
    }

    $vis_image_link_Ar = explode("\n", $vis_images);
    $vis_image_link_Ar = array_filter($vis_image_link_Ar, 'trim');
    $cnt = 0;
    $vis_image = "";
    $vis_link = "#";
    $vis_target = "";
    $vis_html = "";
    $vis_js = "";

    if ($vis_random == "YES") {
      shuffle($vis_image_link_Ar);
    }

    foreach ($vis_image_link_Ar as $vis_image_link) {
      $vis_image_link = preg_replace("/\r|\n/", "", $vis_image_link);
      if (strpos($vis_image_link, ']-[') !== FALSE) {
        $vis_image_link = explode("]-[", $vis_image_link);
        $vis_image = $vis_image_link[0];
        $vis_link = $vis_image_link[1];
      }
      elseif (strpos($vis_image_link, '] - [') !== FALSE) {
        $vis_image_link = explode("] - [", $vis_image_link);
        $vis_image = $vis_image_link[0];
        $vis_link = $vis_image_link[1];
      }
      elseif (strpos($vis_image_link, ']- [') !== FALSE) {
        $vis_image_link = explode("]- [", $vis_image_link);
        $vis_image = $vis_image_link[0];
        $vis_link = $vis_image_link[1];
      }
      elseif (strpos($vis_image_link, '] -[') !== FALSE) {
        $vis_image_link = explode("] -[", $vis_image_link);
        $vis_image = $vis_image_link[0];
        $vis_link = $vis_image_link[1];
      }
      else {
        if ($vis_image_link <> "") {
          $vis_image = $vis_image_link;
          $vis_link = "#";
        }
      }

      $vis_image = ltrim($vis_image, '[');
      $vis_image = rtrim($vis_image, ']');
      $vis_link = ltrim($vis_link, '[');
      $vis_link = rtrim($vis_link, ']');
      $dis_height = $vis_height . "px";
      $vis_html = $vis_html . "<div class='cas_div' style='height:$dis_height;padding:2px 0px 2px 0px;'>";
      $vis_html = $vis_html . "<a style='text-decoration:none' target='$vis_target' class='cas_div' href='$vis_link'><img border='0' src='$vis_image'></a>";
      $vis_html = $vis_html . "</div>";
      $vis_js = $vis_js . "ivrss_array[$cnt] = '<div class=\'cas_div\' style=\'height:$dis_height;padding:2px 0px 2px 0px;\'><a style=\'text-decoration:none\'  target=\'$vis_target\' href=\'$vis_link\'><img border=\'0\' src=\'$vis_image\'></a></div>'; ";
      $cnt++;
    }

    $vis_scrollerheight = $vis_height + 4;
    if ($cnt >= $vis_display) {
      $cnt = $vis_display;
      $vis_height = ($vis_height * $vis_display);
    }
    else {
      $cnt = $cnt;
      $vis_height = ($cnt * $vis_height);
    }
    $vis_height1 = $vis_height . "px";

    $vis_dtl = "";
    $vis_dtl = $vis_dtl . '<div style="padding-top:8px;padding-bottom:8px;">';
    $vis_dtl = $vis_dtl . '<div style="text-align:left;vertical-align:middle;text-decoration: none;overflow: hidden; position: relative; margin-left: 1px; height:' . $vis_height1 . ';" id="ivrss_holder1">';
    $vis_dtl = $vis_dtl . $vis_html;
    $vis_dtl = $vis_dtl . "</div>";
    $vis_dtl = $vis_dtl . "</div>";
    $vis_dtl = $vis_dtl . '<script type="text/javascript">';
    $vis_dtl = $vis_dtl . "var ivrss_array	= new Array();";
    $vis_dtl = $vis_dtl . "var ivrss_obj	= '';";
    $vis_dtl = $vis_dtl . "var ivrss_scrollPos 	= '';";
    $vis_dtl = $vis_dtl . "var ivrss_numScrolls	= '';";
    $vis_dtl = $vis_dtl . "var ivrss_heightOfElm = '" . $vis_scrollerheight . "';";
    $vis_dtl = $vis_dtl . "var ivrss_numberOfElm = '" . $vis_display . "';";
    $vis_dtl = $vis_dtl . "var ivrss_speed = '" . $vis_speed . "';";
    $vis_dtl = $vis_dtl . "var ivrss_waitseconds = '" . $vis_waitsec . "';";
    $vis_dtl = $vis_dtl . "var ivrss_scrollOn 	= 'true';";
    $vis_dtl = $vis_dtl . 'function ivrss_createscroll() ';
    $vis_dtl = $vis_dtl . '{';
    $vis_dtl = $vis_dtl . $vis_js;
    $vis_dtl = $vis_dtl . "ivrss_obj	= document.getElementById('ivrss_holder1');";
    $vis_dtl = $vis_dtl . "ivrss_obj.style.height = (ivrss_numberOfElm * ivrss_heightOfElm) + 'px';";
    $vis_dtl = $vis_dtl . '(function () { "use strict"; ivrss_content(); })()';
    $vis_dtl = $vis_dtl . "}";
    $vis_dtl = $vis_dtl . "</script>";
    $vis_dtl = $vis_dtl . '<script type="text/javascript">';
    $vis_dtl = $vis_dtl . "ivrss_createscroll();";
    $vis_dtl = $vis_dtl . "</script>";

    return $vis_dtl;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['vis_display'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display image count'),
      '#description' => $this->t('Enter number of images you want to display at the same time in the scroll, enter only number.  (Example: 2)'),
      '#default_value' => isset($config['vis_display']) ? $config['vis_display'] : '2',
    ];

    $form['vis_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height of each image (px)'),
      '#description' => $this->t('Enter height of each images in the scroll, enter only number. (Example: 170)'),
      '#default_value' => isset($config['vis_height']) ? $config['vis_height'] : '170',
    ];

    $form['vis_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scrolling speed'),
      '#description' => $this->t('Enter how fast you want to scroll, enter only number. (Example: 2)'),
      '#default_value' => isset($config['vis_speed']) ? $config['vis_speed'] : '2',
    ];

    $form['vis_waitsec'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seconds to wait'),
      '#description' => $this->t('Enter how many seconds you want the wait to scroll. enter only number. (Example: 2)'),
      '#default_value' => isset($config['vis_waitsec']) ? $config['vis_waitsec'] : '2',
    ];

    $options = [
      'YES' => $this->t('YES'),
      'NO' => $this->t('NO'),
    ];

    $form['vis_random'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select random display'),
      '#options' => $options,
      '#description' => $this->t('This option is to retrieve the images in random order. (Example: YES)'),
      '#default_value' => $config['vis_random'],
    ];

    $form['vis_images'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter image details'),
      '#description' => $this->t('Please enter image url. One image url per line. [Where is the picture located on the internet]-[When someone clicks on the picture, where do you want to send them] <br /> Example : [http://www.gopiplus.com/work/wp-content/uploads/pluginimages/250x167/250x167_1.jpg]-[http://www.gopiplus.com/]'),
      '#default_value' => isset($config['vis_images']) ? $config['vis_images'] : '[http://www.gopiplus.com/work/wp-content/uploads/pluginimages/250x167/250x167_1.jpg]-[http://www.gopiplus.com/]',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['vis_display'] = $form_state->getValue('vis_display');
    $this->configuration['vis_height'] = $form_state->getValue('vis_height');
    $this->configuration['vis_speed'] = $form_state->getValue('vis_speed');
    $this->configuration['vis_waitsec'] = $form_state->getValue('vis_waitsec');
    $this->configuration['vis_random'] = $form_state->getValue('vis_random');
    $this->configuration['vis_images'] = $form_state->getValue('vis_images');
  }

}
