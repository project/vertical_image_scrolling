/**
 * @name		Vertical image scrolling
 * @author		Gopi RAMASAMY
 * @version 	1.0
 * @url			http://www.gopiplus.com/extensions/2017/04/drupal-module-vertical-image-scrolling/
 * @license		MIT License
 */

function ivrss_scroll() {
  'use strict';
  ivrss_obj.scrollTop = ivrss_obj.scrollTop + 1;
  ivrss_scrollPos++;
  if ((ivrss_scrollPos % ivrss_heightOfElm) === 0) {
    ivrss_numScrolls--;
    if (ivrss_numScrolls === 0) {
      ivrss_obj.scrollTop = '0';
      ivrss_content();
    }
    else {
      if (ivrss_scrollOn === 'true') {
        ivrss_content();
      }
    }
  }
  else {
    var speed = 60 - (ivrss_speed * 10);
    // setTimeout('ivrss_scroll();', speed);
    setTimeout(function () { ivrss_scroll(); }, speed);
    // setTimeout(function () { "use strict"; ivrss_scroll(); }, speed);
  }
}

var ivrss_Num = 0;
function ivrss_content() {
  'use strict';
  var w_vsrp;
  var i_vsrp;
  var tmp_vsrp = '';
  w_vsrp = ivrss_Num - parseInt(ivrss_numberOfElm);
  if (w_vsrp < 0) {
    w_vsrp = 0;
  }
  else {
    w_vsrp = w_vsrp % ivrss_array.length;
  }
  var elementsTmp_vsrp = parseInt(ivrss_numberOfElm) + 1;
  for (i_vsrp = 0; i_vsrp < elementsTmp_vsrp; i_vsrp++) {
    tmp_vsrp += ivrss_array[w_vsrp % ivrss_array.length];
    w_vsrp++;
  }
  ivrss_obj.innerHTML = tmp_vsrp;
  ivrss_Num = w_vsrp;
  ivrss_numScrolls = ivrss_array.length;
  ivrss_obj.scrollTop = '0';
  // setTimeout('ivrss_scroll();', ivrss_waitseconds * 1000);
  setTimeout(function () { ivrss_scroll(); }, ivrss_waitseconds * 1000);
  // setTimeout(function () { "use strict"; ivrss_scroll(); }, ivrss_waitseconds * 1000);
}
